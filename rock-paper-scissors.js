// menangkap pilihan computer
// membuat bilangan random
function getPilihanComputer() {
    const comp = Math.random();
    if (comp < 0.34) return 'batu';
    if (comp >= 0.34 && comp < 0.67) return 'kertas';
    return 'gunting';
}

// tentukan rules
function getHasil (comp, player) {
if (player == comp) return 'PLAYER 1 \n SERI';
if (player == 'batu') return (comp == 'kertas') ? 'PLAYER 1 \n KALAH' : 'PLAYER 1 \n MENANG';
if (player == 'kertas') return (comp == 'batu') ? 'PLAYER 1 \n MENANG' : 'PLAYER 1 \n KALAH';
if (player == 'gunting') return (comp == 'kertas') ? 'PLAYER 1 \n MENANG' : 'PLAYER 1 \n KALAH';
}



// menangkap pilihan player
const pilihanPlayer = document.querySelectorAll('#player img');
pilihanPlayer.forEach(function(pil) {
    pil.addEventListener('click', function() {
        const pilihanComputer = getPilihanComputer();
        const pilihanPlayer = pil.className;
        const hasil = getHasil(pilihanComputer, pilihanPlayer);
    
        function clickComputer () {
        if  (pilihanComputer == 'batu') return clickComputer.querySelector('.comp-batu').onclick;
        if  (pilihanComputer == 'kertas') return clickComputer.querySelector('.comp-kertas').onclick;
        else return clickComputer.querySelector('.comp-gunting').onclick;
        }

        const info = document.querySelector('.container9 .col-2');
        info.innerHTML = hasil;
        info.style.backgroundColor = 'green';
        info.style.color = 'white';
        info.style.transform = 'rotate(-20deg)';
        info.style.fontSize = '40px';
        info.style.justifyItem = 'center';
        info.style.textAlign = 'center';

//         const reset = document.querySelector('.reset');
//         reset.addEventListener('click', function() {
//         hasil.reset();
// })

    })
})

